# Statistical Approaches to the Model Comparison Task in Learning Analytics

This repository contains companion code to replicate the analysis in "Statistical Approaches to the Model Comparison Task in Learning Analytics," submitted to the LAK 2017 Workshop on Methodology in Learning Analytics (MLA).

The code reads a file of appended feature data (similar to that of Xing 2016, "Temporal predication of dropouts in MOOCs: Reaching the low hanging fruit through stacking generalization"), trains two models to predict learner dropout in the third week of the course, and calculates p-values using test statistics under two different methods outlined in the paper (corrected 10x10 cross-validation, and the sorted runs sampling scheme with a paired t-test).

For readers specifically interested in the implementations of the test statistics (and not the data wranglging or model training/testing), see the ```corrected_cv()``` and ```sorted_runs()``` functions. You can also load all of these functions into your own R script using:

```
source('methlab_demo.Rmd')
```

For more details on the methodology in this code and the motivation behind it, see the full paper. Contact Josh Gardner, jpgard@umich.edu, with questions or comments on the code, requisite data format, or for Python code to extract a similar featureset from a Coursera clickstream log file.
